﻿<#

.SYNOPSIS
This is a simple Powershell script to explain how to create help 

.DESCRIPTION
Function this skripts:
-user passes time he/she goes to bed and wakes up at;
-depending on that info script writes whether the user is an owl or a lark; 
-script also writes if the user has enough sleep (8 hours or more) 
- use try-catch blocks to process input 


.EXAMPLE
./HelloWorld.ps1

.NOTES
Put some notes here.

.LINK
http://kevinpelgrims.wordpress.com

#>


param( $Hour,$Minute, $Second ) 
 [datetime]$t
 # Night
 Write-Output " evening"
 try {
# [int32][validaterange(0,31)]$Day = $(Read-Host "Enter day") 
[int32][validaterange(0,24)]$Hour = $(Read-Host "Enter Hour") 
[int32][validaterange(0,59)]$Minute = $(Read-Host "Enter Minute")
[int32][validaterange(0,59)]$Second = $(Read-Host "Enter second")
[datetime]$timenight = Get-Date -Format T  -Hour $Hour -Minute $Minute -Second $Second
#$timenight.ToString("HH-mm-ss")
 #Morning
Write-Output " morning"
#[int32][validaterange(0,31)]$Day = $(Read-Host "Enter day") 
[int32][validaterange(0,24)]$Hour = $(Read-Host "Enter Hour") 
[int32][validaterange(0,59)]$Minute = $(Read-Host "Enter Minute")
[int32][validaterange(0,59)]$Second = $(Read-Host "Enter second")
[datetime]$timem = Get-Date -Format T  -Hour $Hour -Minute $Minute -Second $Second
#$timem.ToString("HH-mm-ss")
} 
catch [Exception ] {    
Write-Output "Something threw an exception"
 }

 $t = $timem.AddHours(-8)  
if ($t.Hour -le $timenight.Hour )
{
Write-Output "YoU need sleep more"
}

if($timem.Hour -le 6)
{Write-Output "YoU are lark" }

if ($timenight.Hour -ge 22)
{Write-Output "YoU are owl"}
 if ($timem.Hour -le 6 -and $timenight.Hour -ge 22)
 {{Write-Output "YoU SLEEP VERY FEW!!!!!!!!!!!!!"}}



